#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX 50

struct serie
{
    int codigo;
    char titulo[MAX];
    int quantTemporadas;
    struct listaTemporadas *temporadasInicio;
};

struct arvSeries
{
    struct serie info;
    struct arvSeries *esquerda;
    struct arvSeries *direita;
};

typedef struct arvSeries ArvSeries;
typedef struct serie Serie;

struct participante
{
    char personagem[MAX];
    char artista[MAX];
    char descricao[MAX];
};
typedef struct participante Participante;

struct arvParticipante
{
    Participante info;
    struct arvParticipante *esquerda;
    struct arvParticipante *direita;
};
typedef struct arvParticipante ArvParticipante;

struct temporada
{
    int numero;
    char titulo[MAX];
    int quantEpisodios;
    int ano;
    ArvParticipante *participantesRaiz;
};

struct listaTemporadas
{
    struct temporada info;
    struct listaTemporadas *prox;
};

typedef struct listaTemporadas ListaTemporadas;
typedef struct temporada Temporada;

/*recebe como parametros o numero da temporada, o titulo da temporada, a quantidade de episodios, o ano,
e os participantes dessa temporada
retorna a temporada com os dados adicionados. 
*/
Temporada criarTemporada(int numero, char titulo[MAX], int quantEpisodios, int ano, ArvParticipante *arvParticipante)
{
    Temporada temporada;
    temporada.numero = numero;
    strcpy(temporada.titulo, titulo);
    temporada.quantEpisodios = quantEpisodios;
    temporada.ano = ano;
    temporada.participantesRaiz = arvParticipante;
    return temporada;
}

/*recebe como parametros o numero da temporada, o titulo da temporada, a quantidade de episodios, o ano,
e os participantes dessa temporada
retorna uma lista de temporada com os dados adicionados. 
*/
ListaTemporadas *criarListaTemporadas(int numero, char titulo[MAX], int quantEpisodios, int ano, ArvParticipante *arvParticipante)
{
    Temporada temporada = criarTemporada(numero, titulo, quantEpisodios, ano, arvParticipante);
    ListaTemporadas *no = (ListaTemporadas *)calloc(1, sizeof(ListaTemporadas));
    no->info = temporada;
    no->prox = NULL;
    return no;
}

/* insere o no na lista de temporadas e retorna a lista alterada. 
*/
ListaTemporadas *inserirTemporada(ListaTemporadas *lista, ListaTemporadas *no)
{

    if (lista == NULL)
    {
        lista = no;
    }
    else
    {
        ListaTemporadas *aux;
        for (aux = lista; aux->prox != NULL; aux = aux->prox)
        {
            // Nada.
        }
        aux->prox = no;
    }
    return lista;
}

/* imprime dados de uma temporada 
*/
void imprimirTemporada(Temporada temporada)
{
    printf("\nNumero: %d\n", temporada.numero);
    printf("Titulo: %s\n", temporada.titulo);
    printf("Quantidade de episódios: %d\n", temporada.quantEpisodios);
    printf("Ano %d\n", temporada.ano);
}

/* imprime dados de cada temporada de uma lista de temporadas
*/
void imprimirListaTemporadas(ListaTemporadas *lista)
{
    if (lista == NULL)
    {
        printf("A lista está vazia\n");
    }

    else
    {
        ListaTemporadas *aux;

        for (aux = lista; aux != NULL;)
        {
            imprimirTemporada(aux->info);
            aux = aux->prox;
        }
    }
}

/*recebe como parametros o nome do personagem, o nome do artista e a descrição
retorna um participante com dados adicionados. 
*/
Participante criarParticipante(char personagem[MAX], char artista[MAX], char descricao[MAX])
{
    Participante no;
    strcpy(no.artista, artista);
    strcpy(no.descricao, descricao);
    strcpy(no.personagem, personagem);
    return no;
}

/*recebe como parametros o nome do personagem, o nome do artista e a descrição
retorna uma arvore de participantes com dados adicionados.*/
ArvParticipante *criarArvParticipante(char personagem[MAX], char artista[MAX], char descricao[MAX])
{
    Participante participante = criarParticipante(personagem, artista, descricao);
    ArvParticipante *no = (ArvParticipante *)calloc(1, sizeof(ArvParticipante));
    no->info = participante;
    no->esquerda = NULL;
    no->direita = NULL;
    return no;
}

/*compara a palavra1 e a palavra 2 para averiguar qual vem primeiro em ordem alfabetica.
retorna 1 se a palavra que vem primeiro for palavra1
retorna 2 se a palavra que vem primeiro for palavra2
retorna 0 se as duas palavras possuem os mesmos caracteres
A variavel indica a posição do caracter a ser comparado. Deve receber 0 na chamada da função*/
int compararPalavras(char palavra1[MAX], char palavra2[MAX], int i)
{
    int palavraQueVemPrimeiro = 0;
    if (strlen(palavra1) > i && strlen(palavra2) > i)
    {
        if (tolower(palavra1[i]) == tolower(palavra2[i]))
        {
            palavraQueVemPrimeiro = compararPalavras(palavra1, palavra2, i + 1);
        }
        else
        {
            if (tolower(palavra1[i]) < tolower(palavra2[i]))
                palavraQueVemPrimeiro = 1;
            if (tolower(palavra1[i]) > tolower(palavra2[i]))
                palavraQueVemPrimeiro = 2;
        }
    }
    return palavraQueVemPrimeiro;
}

/* insere o no na arvore de participantes*/
void inserirParticipante(ArvParticipante **raiz, ArvParticipante *no)
{
    if (*raiz == NULL)
    {
        *raiz = no;
    }
    else
    {
        int primeiro = compararPalavras(no->info.personagem, (*raiz)->info.personagem, 0);
        if (primeiro == 1)
        {
            inserirParticipante(&((*raiz)->esquerda), no);
        }
        else
        {
            inserirParticipante(&((*raiz)->direita), no);
        }
    }
}

/* imprime dados de um participante*/
void imprimirParticipante(Participante participante)
{
    printf("\nArtista: %s\n", participante.artista);
    printf("Personagem: %s\n", participante.personagem);
    printf("Descricao: %s\n", participante.descricao);
}

/* imprime os dados de cada participante de uma arvore de participantes*/
void imprimirArvParticipantes(ArvParticipante *raiz)
{
    if (raiz != NULL)
    {
        imprimirParticipante(raiz->info);
        imprimirArvParticipantes(raiz->direita);
        imprimirArvParticipantes(raiz->esquerda);
    }
}

Serie criarSerie(int codigo, char titulo[MAX], int quantTemporadas, ListaTemporadas *listaTemporadas)
{
    Serie no;
    no.codigo = codigo;
    strcpy(no.titulo, titulo);
    no.quantTemporadas = quantTemporadas;
    no.temporadasInicio = listaTemporadas;
    return no;
}

/*recebe como parametros o codigo da serie, o titulo, a quantidade de temporadas e uma lista das temporadas dessa serie
retorna uma arvore de serie com dados adicionados. */
ArvSeries *criarArvSerie(int codigo, char titulo[MAX], int quantTemporadas, ListaTemporadas *listaTemporadas)
{
    Serie serie = criarSerie(codigo, titulo, quantTemporadas, listaTemporadas);
    ArvSeries *no = (ArvSeries *)calloc(1, sizeof(ArvSeries));
    no->info = serie;
    no->esquerda = NULL;
    no->direita = NULL;
    return no;
}

/* insere o no na arvore de series*/
void inserirSerie(ArvSeries **raiz, ArvSeries *no)
{
    if (*raiz == NULL)
    {
        *raiz = no;
    }
    else
    {
        if (no->info.codigo < (*raiz)->info.codigo)
        {
            inserirSerie(&((*raiz)->esquerda), no);
        }
        else
        {
            inserirSerie(&((*raiz)->direita), no);
        }
    }
}

/* imprime os dados de uma serie*/
void imprimirSerie(Serie serie)
{
    printf("\nCodigo: %d\n", serie.codigo);
    printf("titulo: %s\n", serie.titulo);
    printf("Quantidade de temporadas: %d\n", serie.quantTemporadas);
}

/* imprime os dados de cada serie de uma arvore de series*/
void imprimirArvSeries(ArvSeries *raiz)
{
    if (raiz != NULL)
    {
        //in ordem
        imprimirArvSeries(raiz->esquerda);
        imprimirSerie(raiz->info);
        imprimirArvSeries(raiz->direita);
    }
}

/* busca na arvore de participantes 'raiz', qual arvore tem como o personagem 'personagem'.
Se encontrar, adiciona na arvore 'participante'*/
void buscarParticipante(ArvParticipante *raiz, char personagem[MAX], ArvParticipante **participante)
{
    if (raiz != NULL)
    {
        if (strcmp(personagem, raiz->info.personagem) == 0)
        {

            *participante = raiz;
        }
        else
        {
            int primeiro = compararPalavras(personagem, (raiz)->info.personagem, 0);
            if (primeiro == 1)
            {
                buscarParticipante((raiz->esquerda), personagem, participante);
            }
            else
            {
                buscarParticipante((raiz->direita), personagem, participante);
            }
        }
    }
}

/* busca na arvore de series 'raiz', qual arvore tem como codigo 'codigo'.
Se encontrar, adiciona na arvore 'serie'*/
void buscarSerie(ArvSeries *raiz, int codigo, ArvSeries **serie)
{
    if (raiz != NULL)
    {
        if (codigo == raiz->info.codigo)
        {

            *serie = raiz;
        }
        else
        {
            if (codigo < raiz->info.codigo)
            {
                buscarSerie((raiz->esquerda), codigo, serie);
            }
            else
            {
                buscarSerie((raiz->direita), codigo, serie);
            }
        }
    }
}

/* busca na lista de temporadas 'lista', qual lista tem como o numero da temporada 'numero'.
Se encontrar, adiciona na lista 'temporada'*/
void buscarTemporada(ListaTemporadas *lista, int numero, ListaTemporadas **temporada)
{
    if (lista != NULL)
    {
        ListaTemporadas *aux;

        for (aux = lista; aux != NULL && *temporada == NULL; aux = aux->prox)
        {
            if (numero == aux->info.numero)
            {
                *temporada = aux;
            }
        }
    }
}
/*lê as variaveis recebidas como ponteiros*/
void lerSerie(int *codigo, char titulo[MAX], int *quantTemporadas)
{
    printf("\nDigite codigo, titulo e quantidade de temporadas da série:\n");
    scanf("%d", codigo);
    scanf("%s", titulo);
    setbuf(stdin, NULL);
    scanf("%d", quantTemporadas);
}

/*lê as variaveis recebidas como ponteiros*/
void lerTemporada(char titulo[MAX], int *quantEpisodios, int *ano)
{
    printf("\nDigite titulo, quantidade de episodios e ano:\n");
    scanf("%s", titulo);
    setbuf(stdin, NULL);
    scanf("%d", quantEpisodios);
    scanf("%d", ano);
}

/*lê as variaveis recebidas como ponteiros*/
void lerParticipante(char personagem[MAX], char artista[MAX], char descricao[MAX])
{
    printf("\nDigite personagem, artista e descrição do participante:\n");
    scanf("%s", personagem);
    setbuf(stdin, NULL);
    scanf("%s", artista);
    setbuf(stdin, NULL);
    scanf("%s", descricao);
}

/*cadastra uma nova serie, e retorna ela. Essa serie terá temporadas e personagens*/
ArvSeries *cadastroSerieUsuario()
{
    int codigo, quantTemporadas;
    char titulo[MAX];

    lerSerie(&codigo, titulo, &quantTemporadas);
    ListaTemporadas *listaTemporadas = NULL;
    for (int numero = 1; numero <= quantTemporadas; numero++)
    {
        int quantEpisodios, ano;
        char titulo[MAX];
        printf("\nDigite informações da temporada %d:\n", numero);
        lerTemporada(titulo, &quantEpisodios, &ano);
        printf("\nQuantidade de personagens:");
        int quantParticipantes;
        scanf("%d", &quantParticipantes);
        setbuf(stdin, NULL);
        ArvParticipante *arvParticipantes = NULL;
        for (int i = 0; i < quantParticipantes; i++)
        {
            char personagem[MAX], artista[MAX], descricao[MAX];
            lerParticipante(personagem, artista, descricao);
            ArvParticipante *participante = criarArvParticipante(personagem, artista, descricao);
            inserirParticipante(&arvParticipantes, participante);
        }
        ListaTemporadas *temporada = criarListaTemporadas(numero, titulo, quantEpisodios, ano, arvParticipantes);
        listaTemporadas = inserirTemporada(listaTemporadas, temporada);
    }
    ArvSeries *serie = criarArvSerie(codigo, titulo, quantTemporadas, listaTemporadas);
    return serie;
}

/*percorre todas as temporadas da serie arvSeries, cada vez que encontrar um participante com personagem de nome 'personagem',
imprime as informações desse personagem e a temporada.
Se não encontrar nenhum nessa série, imprime nada encontrada*/
void artistasInterpretaramPersonagem(ArvSeries *arvSeries, char personagem[MAX])
{
    int encontrei = 0;
    ListaTemporadas *temporada = arvSeries->info.temporadasInicio;
    for (int i = 1; i <= arvSeries->info.quantTemporadas; i++)
    {
        ArvParticipante *participante = NULL;
        buscarParticipante(temporada->info.participantesRaiz, personagem, &participante);
        if (participante != NULL)
        {
            printf("\nTemporada de número %d:", i);
            encontrei = 1;
            imprimirParticipante(participante->info);
        }

        temporada = temporada->prox;
    }
    if (encontrei == 0)
    {
        printf("Nada encontrado");
    }
}

//Cria uma serie com temporadas e personagens para fins de teste//
ArvSeries *criarSerieBatman()
{
    ArvParticipante *arvParticipantes = NULL;
    ArvParticipante *participante = criarArvParticipante("Batman", "joao", "heroi");
    inserirParticipante(&arvParticipantes, participante);
    participante = criarArvParticipante("Coringa", "josé", "vilao");
    inserirParticipante(&arvParticipantes, participante);

    ListaTemporadas *listaTemporadas = NULL;
    ListaTemporadas *temporada = criarListaTemporadas(1, "temporada1", 21, 2000, arvParticipantes);
    listaTemporadas = inserirTemporada(listaTemporadas, temporada);

    arvParticipantes = NULL;
    participante = criarArvParticipante("Coringa", "Pedro", "vilao");
    inserirParticipante(&arvParticipantes, participante);

    temporada = criarListaTemporadas(2, "temporada2", 30, 2001, arvParticipantes);
    listaTemporadas = inserirTemporada(listaTemporadas, temporada);

    return criarArvSerie(4, "Batman", 2, listaTemporadas);
}

//Cria uma serie com temporadas e personagens para fins de teste//
ArvSeries *criarSerieMulherMaravilha()
{
    ArvParticipante *arvParticipantes = NULL;
    ArvParticipante *participante = criarArvParticipante("MulherMaravilha", "Maria", "heroina");
    inserirParticipante(&arvParticipantes, participante);

    ListaTemporadas *listaTemporadas = NULL;
    ListaTemporadas *temporada = criarListaTemporadas(1, "temporadaUnica", 21, 2000, arvParticipantes);
    listaTemporadas = inserirTemporada(listaTemporadas, temporada);

    return criarArvSerie(1, "MulherMaravilha", 1, listaTemporadas);
}

void menu()
{
    printf("\n\nMenu\n\nEscolha uma opção:\n");
    printf("1- imprimir todas as séries\n");
    printf("2- imprimir temporadas\n");
    printf("3- imprimir personagens\n");
    printf("4- imprimir artistas que interpretaram um personagem\n");
    printf("0- sair\n");
}

void main()
{
    ArvSeries *raizSeries = NULL, *buscaSerie;
    ListaTemporadas *buscaTemporada;
    int op, codigo;

    do
    {
        printf("Escolha uma opção:\n");
        printf("1- Eu mesmo quero cadastrar as séries\n");
        printf("2- Desejo que o sistema cadastre\n");
        scanf("%d", &op);
    } while (op != 1 && op != 2);
    if (op == 1)
    {
        int quantSeries;
        printf("Digite a quantidade de séries que deseja cadastrar:\n");
        scanf("%d", &quantSeries);

        for (int i = 0; i < quantSeries; i++)
        {
            printf("\n*Serie*\n");
            ArvSeries *serie = cadastroSerieUsuario(&raizSeries);
            inserirSerie(&raizSeries, serie);
        }
    }
    else
    {
        ArvSeries *serie = criarSerieBatman();
        inserirSerie(&raizSeries, serie);
        serie = criarSerieMulherMaravilha();
        inserirSerie(&raizSeries, serie);
    }

    do
    {
        menu();
        scanf("%d", &op);
        switch (op)
        {
        case 1:
            imprimirArvSeries(raizSeries);
            break;
        case 2:
            buscaSerie = NULL;

            printf("Digite o código da serie:");
            scanf("%d", &codigo);
            buscarSerie(raizSeries, codigo, &buscaSerie);
            if (buscaSerie == NULL)
            {
                printf("Nada encontrado\n");
            }
            else
            {
                imprimirListaTemporadas(buscaSerie->info.temporadasInicio);
            }
            break;
        case 3:
            buscaSerie = NULL;
            printf("Digite o código da serie:");
            scanf("%d", &codigo);
            buscarSerie(raizSeries, codigo, &buscaSerie);
            if (buscaSerie == NULL)
            {
                printf("Nada encontrado\n");
            }
            else
            {
                printf("Digite o numero da temporada:");
                scanf("%d", &codigo);
                buscaTemporada = NULL;
                buscarTemporada(buscaSerie->info.temporadasInicio, codigo, &buscaTemporada);
                if (buscaTemporada == NULL)
                {
                    printf("Nada encontrado\n");
                }
                else
                {
                    imprimirArvParticipantes(buscaTemporada->info.participantesRaiz);
                }
            }
            break;
        case 4:
            printf("Digite o código da serie:");
            scanf("%d", &codigo);
            buscarSerie(raizSeries, codigo, &buscaSerie);
            if (buscaSerie == NULL)
            {
                printf("Nada encontrado\n");
            }
            else
            {
                char personagem[MAX];
                printf("Digite o nome do personagem:");
                scanf("%s", personagem);
                setbuf(stdin, NULL);
                artistasInterpretaramPersonagem(buscaSerie, personagem);
            }
            break;
        }
    } while (op != 0);
}