#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX 50

int compararPalavras(char palavra1[MAX], char palavra2[MAX], int i)
{
    int palavraQueVemPrimeiro = 0;
    if (strlen(palavra1) > i && strlen(palavra2) > i)
    {
        if (tolower(palavra1[i]) == tolower(palavra2[i]))
        {
            palavraQueVemPrimeiro = compararPalavras(palavra1, palavra2, i + 1);
        }
        else
        {
            if (tolower(palavra1[i]) < tolower(palavra2[i]))
                palavraQueVemPrimeiro = 1;
            if (tolower(palavra1[i]) > tolower(palavra2[i]))
                palavraQueVemPrimeiro = 2;
        }
    }
    return palavraQueVemPrimeiro;
}

void main()
{
    int n = compararPalavras("darice", "Weliton", 0);
    printf("%d", n);
}
