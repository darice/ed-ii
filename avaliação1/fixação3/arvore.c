#include <stdio.h>
#include <stdlib.h>

struct arvore
{
    struct arvore *esquerda;
    int info;
    struct arvore *direita;
};

typedef struct arvore Arvore;

// Arvore *inserirNo(Arvore *raiz, Arvore *no){
//     if(raiz == NULL){
//         raiz = no;
//     }
//     else{
//         if(no < raiz){
//             inserirNo(raiz->esquerda, no);
//         }
//         else if(no > raiz){
//             inserirNo(raiz->direita, no);
//         }
//     }
//     return raiz;
// }
void inserirNo(Arvore **raiz, Arvore *no)
{
    if (*raiz == NULL)
    {
        *raiz = no;
    }
    else
    {
        if (no->info < (*raiz)->info)
        {
            inserirNo(&((*raiz)->esquerda), no);
        }
        else if (no->info > (*raiz)->info)
        {
            inserirNo(&((*raiz)->direita), no);
        }
    }
}

Arvore *criarNo(int n)
{
    Arvore *no = (Arvore *)calloc(1, sizeof(Arvore));
    no->info = n;
    no->esquerda = NULL;
    no->direita = NULL;
    return no;
}

void buscar(Arvore *raiz, int elemento, int *encontrou)
{

    if (raiz != NULL)
    {
        if (elemento == raiz->info)
        {
            *encontrou = 1;
        }
        else
        {
            if (elemento < raiz->info)
                buscar(raiz->esquerda, elemento, encontrou);
            else
                buscar(raiz->direita, elemento, encontrou);
        }
    }
}

Arvore *removerFolha(Arvore *no)
{
    Arvore *aux = no;
    no = NULL;
    free(aux);
    return no;
}
Arvore *removerNoUnicoFilho(Arvore *no)
{
    Arvore *filho;
    if (no->direita == NULL)
        filho = no->esquerda;
    else
        filho = no->direita;
    free(no);
    return filho;
}

Arvore *noMaiorElemento(Arvore *raiz)
{
    if (raiz->direita != NULL)
        noMaiorElemento(raiz->direita);
    else
        return raiz;
}

Arvore *removerNoDoisfilhos(Arvore *no)
{
    Arvore *maior = noMaiorElemento(no->esquerda);
    maior->direita = no->direita;
    Arvore *aux = no;
    no = aux->esquerda;
    free(aux);
    return no;
}

int quantFilhos(Arvore *no)
{
    int filhos;
    if (no->direita == NULL && no->esquerda == NULL)
        filhos = 0;
    else if (no->direita != NULL && no->esquerda != NULL)
        filhos = 2;
    else
        filhos = 1;
    return filhos;
}

Arvore *remover(Arvore *raiz, int elemento, int *encontrou)
{
    if (raiz != NULL)
    {
        if (elemento == raiz->info)
        {
            *encontrou = 1;
            if (quantFilhos(raiz) == 0)
                raiz = removerFolha(raiz);
            else if (quantFilhos(raiz) == 1)
                raiz = removerNoUnicoFilho(raiz);
            else
                raiz = removerNoDoisfilhos(raiz);
        }
        else
        {
            if (elemento < raiz->info)
                raiz->esquerda = remover(raiz->esquerda, elemento, encontrou);
            else
                raiz->direita = remover(raiz->direita, elemento, encontrou);
        }
    }
    return raiz;
}
Arvore *removerW(Arvore *raiz, int elemento, int *encontrou)
{
    if (raiz != NULL)
    {
        if (elemento == raiz->info)
        {
            *encontrou = 1;
            Arvore *esq = raiz->esquerda;
            Arvore *dir = raiz->direita;
            raiz = removerFolha(raiz);

            inserirNo(&raiz, esq);
            inserirNo(&raiz, dir);
        }
        else
        {
            if (elemento < raiz->info)
                raiz->esquerda = remover(raiz->esquerda, elemento, encontrou);
            else
                raiz->direita = remover(raiz->direita, elemento, encontrou);
        }
    }
    return raiz;
}

void imprimirArvore(Arvore *raiz)
{
    if (raiz != NULL)
    {
        printf("%d ", raiz->info);
        if (raiz->esquerda != NULL)
            printf("%d,", raiz->esquerda->info);
        else
            printf("-,");
        if (raiz->direita != NULL)
            printf("%d", raiz->direita->info);
        else
            printf("- ");
        printf("\n");
        imprimirArvore(raiz->esquerda);
        imprimirArvore(raiz->direita);
    }
}

void main()
{
    Arvore *raiz, *no;
    raiz = NULL;

    no = criarNo(30);
    inserirNo(&raiz, no);
    no = criarNo(60);
    inserirNo(&raiz, no);
    no = criarNo(90);
    inserirNo(&raiz, no);
    no = criarNo(40);
    inserirNo(&raiz, no);
    no = criarNo(20);
    inserirNo(&raiz, no);
    no = criarNo(50);
    inserirNo(&raiz, no);
    no = criarNo(80);
    inserirNo(&raiz, no);
    no = criarNo(10);
    inserirNo(&raiz, no);
    no = criarNo(45);
    inserirNo(&raiz, no);
    no = criarNo(35);
    inserirNo(&raiz, no);
    no = criarNo(55);
    inserirNo(&raiz, no);
    no = criarNo(53);
    inserirNo(&raiz, no);

    imprimirArvore(raiz);

    int encontrou = 0;

    raiz = remover(raiz, 60, &encontrou);
    raiz = remover(raiz, 90, &encontrou);
    raiz = remover(raiz, 10, &encontrou);
    raiz = remover(raiz, 40, &encontrou);
    raiz = remover(raiz, 30, &encontrou);
    raiz = remover(raiz, 50, &encontrou);
    raiz = remover(raiz, 35, &encontrou);

    printf("encontrou? %d\n", encontrou);

    printf("\n");
    imprimirArvore(raiz);
    printf("raiz: %d\n", (raiz)->info);
}