#include <stdio.h>
#include <stdlib.h>

struct arvore
{
    struct arvore *esquerda;
    int info;
    struct arvore *direita;
};

typedef struct arvore Arvore;

/*recebo um no e insiro na arvore*/
void inserirNo(Arvore **raiz, Arvore *no)
{
    if (*raiz == NULL)
    {
        *raiz = no;
    }
    else
    {
        if (no->info < (*raiz)->info)
        {
            inserirNo(&((*raiz)->esquerda), no);
        }
        else if (no->info > (*raiz)->info)
        {
            inserirNo(&((*raiz)->direita), no);
        }
    }
}

/*crio um no com info n e retorno o nó*/
Arvore *criarNo(int n)
{
    Arvore *no = (Arvore *)calloc(1, sizeof(Arvore));
    no->info = n;
    no->esquerda = NULL;
    no->direita = NULL;
    return no;
}

/*busco na arvore o no com info='elemento', se encontro retorno o no, se não, retorno NULL*/
Arvore *buscar(Arvore *raiz, int elemento)
{
    Arvore *no;
    if (raiz != NULL)
    {
        if (elemento == raiz->info)
        {
            no = raiz;
        }
        else
        {
            if (elemento < raiz->info)
                no = buscar(raiz->esquerda, elemento);
            else
                no = buscar(raiz->direita, elemento);
        }
    }
    else
    {
        no = NULL;
    }
    return no;
}

/*recebo a arvore raiz e calculo em '*quantNos' a quantidade de nós da arvore e em '*quantRamos' a quantidade de ramos da árvore
Na chamada da função *quantNos e *quantRamos devem receber 0*/
void quantNosRamos(Arvore *raiz, int *quantNos, int *quantRamos)
{
    if (raiz != NULL)
    {
        (*quantNos)++;
        quantNosRamos(raiz->esquerda, quantNos, quantRamos);
        quantNosRamos(raiz->direita, quantNos, quantRamos);
    }
    *quantRamos = *quantNos - 1;
}

/*recebo a arvore raiz e retorno a quantidade de nós descendentes dela*/
int quantDescendentes(Arvore *raiz)
{
    int descendentes = 0;
    if (raiz != NULL)
    {
        if (raiz->esquerda != NULL)
        {
            descendentes += quantDescendentes(raiz->esquerda) + 1;
        }
        if (raiz->direita != NULL)
        {
            descendentes += quantDescendentes(raiz->direita) + 1;
        }
    }
    return descendentes;
}

/*recebo a raiz e um elemento. Retorno a profundidade do nó que possui info='elemento'
*encontrou retorna 1 quando o elemento for encontrado na arvore*/
int profundidadeElemento(Arvore *raiz, int elemento, int *encontrou)
{
    int profundidade = 0;
    if (raiz != NULL)
    {
        if (elemento == raiz->info)
        {
            *encontrou = 1;
        }
        else
        {
            profundidade = 1;
            if (elemento < raiz->info)
                profundidade += profundidadeElemento(raiz->esquerda, elemento, encontrou);
            else
                profundidade += profundidadeElemento(raiz->direita, elemento, encontrou);
        }
    }
    return profundidade;
}

/*recebo um nó e retorno a altura dele*/
int alturaElemento(Arvore *raiz)
{
    int altura = 0;

    if (raiz == NULL)
    {
        return 0;
    }
    if (raiz->esquerda != NULL || raiz->direita != NULL)
    {
        altura = 1;
        int alt_esq = alturaElemento(raiz->esquerda);
        int alt_dir = alturaElemento(raiz->direita);
        if (alt_esq > alt_dir)
            altura += alt_esq;
        else
            altura += alt_dir;
    }
    return altura;
}

void main()
{
    Arvore *raiz, *no;
    raiz = NULL;

    no = criarNo(30);
    inserirNo(&raiz, no);
    no = criarNo(60);
    inserirNo(&raiz, no);
    no = criarNo(90);
    inserirNo(&raiz, no);
    no = criarNo(40);
    inserirNo(&raiz, no);
    no = criarNo(20);
    inserirNo(&raiz, no);
    no = criarNo(50);
    inserirNo(&raiz, no);
    no = criarNo(80);
    inserirNo(&raiz, no);
    int nos = 0;
    int ramos = 0;
    quantNosRamos(raiz, &nos, &ramos);
    printf("nos da arvore: %d\n", nos);
    printf("ramos da arvore: %d\n", ramos);

    int elemento = 60;

    int encontrou = 0;
    Arvore *busca = buscar(raiz, elemento);
    if (busca != NULL)
    {
        printf("\nelemento: %d\n", elemento);
        int profundidade = profundidadeElemento(raiz, elemento, &encontrou);
        printf("profundidade: %d\n", profundidade);
        int altura = alturaElemento(busca);
        printf("altura: %d\n", altura);
        int descendentes = quantDescendentes(busca);
        printf("descendentes: %d\n", descendentes);
    }
    else
    {
        printf("Não encontrado!\n");
    }

    
}