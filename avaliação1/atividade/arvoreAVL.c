#include <stdio.h>
#include <stdlib.h>

struct arvore
{
    struct arvore *esquerda;
    int info;
    struct arvore *direita;
    int altura;
};

typedef struct arvore Arvore;

Arvore *girarHorario(Arvore *raiz)
{
    Arvore *no = raiz->esquerda;
    raiz->esquerda = no->direita;
    no->direita = raiz;
    return no;
}

Arvore *girarAntiHorario(Arvore *raiz)
{
    Arvore *no = raiz->direita;
    raiz->direita = no->esquerda;
    no->esquerda = raiz;
    return no;
}

int fatorBalanceamento(Arvore *raiz)
{
    int fator = 0, esq, dir;
    if (raiz->esquerda == NULL)
        esq = -1;
    else
        esq = raiz->esquerda->altura;
    if (raiz->direita == NULL)
        dir = -1;
    else
        dir = raiz->direita->altura;
    fator = esq - dir;
    return fator;
}

//uso a informação guardada em raiz.altura dos filhos de raiz
int alturaElemento(Arvore *raiz)
{
    int altura = 1;
    int esquerda = -1;
    int direita = -1;

    if (raiz->esquerda != NULL)
        esquerda = raiz->esquerda->altura;
    if (raiz->direita != NULL)
        direita = raiz->direita->altura;
    altura = 1;
    if (esquerda > direita)
        altura += esquerda;
    else
        altura += direita;

    return altura;
}

//não está sendo usada
int alturaComCalculo(Arvore *raiz)
{
    int altura = 0;

    if (raiz == NULL)
    {
        return 0;
    }
    if (raiz->esquerda != NULL || raiz->direita != NULL)
    {
        altura = 1;
        int alt_esq = alturaComCalculo(raiz->esquerda);
        int alt_dir = alturaComCalculo(raiz->direita);
        if (alt_esq > alt_dir)
            altura += alt_esq;
        else
            altura += alt_dir;
    }
    return altura;
}

int calcularAlturaCadaElementoGalho(Arvore *raiz)
{
    int altura = -1;
    int direita = 0;
    int esquerda = 0;
    if (raiz != NULL)
    {
        altura = 1;

        direita += calcularAlturaCadaElementoGalho(raiz->direita);
        esquerda += calcularAlturaCadaElementoGalho(raiz->esquerda);
        if (esquerda > direita)
        {
            altura += esquerda;
            raiz->altura = altura;
        }
        else
        {
            altura += direita;
            raiz->altura = altura;
        }
    }
    return altura;
}

Arvore *balancear(Arvore *raiz)
{
    if (fatorBalanceamento(raiz) > 0) //positivo
    {
        if (fatorBalanceamento(raiz->esquerda) < 0) //negativo
        {
            raiz->esquerda = girarAntiHorario(raiz->esquerda);
        }
        raiz = girarHorario(raiz);
    }
    else if (fatorBalanceamento(raiz) < 0) //negativo
    {
        if (fatorBalanceamento(raiz->direita) > 0) //positivo
        {
            raiz->direita = girarHorario(raiz->direita);
        }
        raiz = girarAntiHorario(raiz);
    }

    return raiz;
}

void inserirNo(Arvore **raiz, Arvore *no)
{
    if (*raiz == NULL)
    {
        *raiz = no;
    }
    else
    {
        if (no->info < (*raiz)->info)
        {
            inserirNo(&((*raiz)->esquerda), no);
        }
        else if (no->info > (*raiz)->info)
        {
            inserirNo(&((*raiz)->direita), no);
        }
        (*raiz)->altura = alturaElemento(*raiz);
        if (abs(fatorBalanceamento(*raiz)) > 1)
        {
            *raiz = balancear(*raiz);
            calcularAlturaCadaElementoGalho(*raiz);
        }
    }
}

Arvore *criarNo(int n)
{
    Arvore *no = (Arvore *)calloc(1, sizeof(Arvore));
    no->info = n;
    no->esquerda = NULL;
    no->direita = NULL;
    no->altura = 0;
    return no;
}

void imprimirArvore(Arvore *raiz)
{
    if (raiz != NULL)
    {
        printf("%d ", raiz->info);
        if (raiz->esquerda != NULL)
            printf("%d,", raiz->esquerda->info);
        else
            printf("-,");
        if (raiz->direita != NULL)
            printf("%d", raiz->direita->info);
        else
            printf("- ");
        printf(" / %d", raiz->altura);
        printf("\n");
        imprimirArvore(raiz->esquerda);
        imprimirArvore(raiz->direita);
    }
}

int profundidadeMaior(Arvore *raiz)
{
    int profundidade = -1;
    if (raiz != NULL)
    {
        profundidade = 1;
        int esquerda = profundidadeMaior(raiz->esquerda);
        int direita = profundidadeMaior(raiz->direita);
        if (esquerda > direita)
        {
            profundidade += esquerda;
        }
        else
        {
            profundidade += direita;
        }
    }
    return profundidade;
}

int ehFolha(Arvore *no)
{
    int folha = 0;
    if (no != NULL)
    {
        if (no->direita == NULL && no->esquerda == NULL)
            folha = 1;
    }
    return folha;
}

int profundidadeMenor(Arvore *raiz)
{
    int profundidade = -1;
    if (raiz != NULL)
    {
        profundidade = 0;

        int esquerda = profundidadeMenor(raiz->esquerda);
        int direita = profundidadeMenor(raiz->direita);
        if (ehFolha(raiz->esquerda))
            profundidade = esquerda + 1;
        else if (ehFolha(raiz->direita) || direita > esquerda)
            profundidade = direita + 1;
        else
        {
            profundidade = esquerda + 1;
        }
    }
    return profundidade;
}

Arvore *removerFolha(Arvore *no)
{
    Arvore *aux = no;
    no = NULL;
    free(aux);
    return no;
}
Arvore *removerNoUnicoFilho(Arvore *no)
{
    Arvore *filho;
    if (no->direita == NULL)
        filho = no->esquerda;
    else
        filho = no->direita;
    free(no);
    return filho;
}

Arvore *noMaiorElemento(Arvore *raiz)
{
    if (raiz->direita != NULL)
        noMaiorElemento(raiz->direita);
    else
        return raiz;
}

Arvore *removerNoDoisfilhos(Arvore *no)
{
    Arvore *maior = noMaiorElemento(no->esquerda);
    maior->direita = no->direita;
    Arvore *aux = no;
    no = aux->esquerda;
    free(aux);
    return no;
}

int quantFilhos(Arvore *no)
{
    int filhos;
    if (no->direita == NULL && no->esquerda == NULL)
        filhos = 0;
    else if (no->direita != NULL && no->esquerda != NULL)
        filhos = 2;
    else
        filhos = 1;
    return filhos;
}

Arvore *remover(Arvore *raiz, int elemento, int *encontrou)
{
    if (raiz != NULL)
    {
        if (elemento == raiz->info)
        {
            *encontrou = 1;
            if (quantFilhos(raiz) == 0)
                raiz = removerFolha(raiz);
            else if (quantFilhos(raiz) == 1)
                raiz = removerNoUnicoFilho(raiz);
            else
                raiz = removerNoDoisfilhos(raiz);
            calcularAlturaCadaElementoGalho(raiz);
        }
        else
        {
            if (elemento < raiz->info)
                raiz->esquerda = remover(raiz->esquerda, elemento, encontrou);
            else
                raiz->direita = remover(raiz->direita, elemento, encontrou);
            if (abs(fatorBalanceamento(raiz)) > 1)
            {
                raiz = balancear(raiz);
                calcularAlturaCadaElementoGalho(raiz);
            }
        }
    }
    return raiz;
}

void main()
{
    Arvore *raiz, *no;
    raiz = NULL;
    inserirNo(&raiz, criarNo(15));
    inserirNo(&raiz, criarNo(18));
    inserirNo(&raiz, criarNo(3));
    inserirNo(&raiz, criarNo(6));
    inserirNo(&raiz, criarNo(4));
    inserirNo(&raiz, criarNo(12));
    inserirNo(&raiz, criarNo(13));
    inserirNo(&raiz, criarNo(1));
    inserirNo(&raiz, criarNo(17));
    inserirNo(&raiz, criarNo(19));
    inserirNo(&raiz, criarNo(10));
    inserirNo(&raiz, criarNo(16));
    
    // inserirNo(&raiz, criarNo(5));
    // inserirNo(&raiz, criarNo(11));
    // inserirNo(&raiz, criarNo(2));
    // inserirNo(&raiz, criarNo(9));
    // inserirNo(&raiz, criarNo(14));
    // inserirNo(&raiz, criarNo(7));
    // inserirNo(&raiz, criarNo(0));
    // inserirNo(&raiz, criarNo(8));
    int encontrou = 0;
   raiz = remover(raiz, 16, &encontrou);
   raiz = remover(raiz, 18, &encontrou);
    raiz = remover(raiz, 17, &encontrou);
     raiz = remover(raiz, 15, &encontrou);

    imprimirArvore(raiz);

    printf("\nmaior: %d\n", profundidadeMaior(raiz));
    printf("menor: %d\n", profundidadeMenor(raiz));
}
