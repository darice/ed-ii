#include <stdio.h>
#include <stdlib.h>
#define PRETO 2
#define VERMELHO 1

struct arvore
{
    int info;
    struct arvore *direita;
    struct arvore *esquerda;
    int cor;
};
typedef struct arvore Arvore;

int cor(Arvore *raiz)
{
    int cor = PRETO;
    if (raiz != NULL)
        cor = raiz->cor;
    return cor;
}

void inverterCor(Arvore *raiz)
{
    if (raiz->cor == VERMELHO)
    {
        raiz->cor = PRETO;
    }
    else
    {
        raiz->cor = VERMELHO;
    }
}

void trocarCor(Arvore *raiz)
{
    inverterCor(raiz);
    if (raiz->esquerda != NULL)
    {
        inverterCor(raiz->esquerda);
    }
    if (raiz->direita != NULL)
    {
        inverterCor(raiz->direita);
    }
}

Arvore *girarHorario(Arvore *raiz)
{
    Arvore *no = raiz->esquerda;
    raiz->esquerda = no->direita;
    no->direita = raiz;
    no->cor = raiz->cor;
    raiz->cor = VERMELHO;
    return no;
}

Arvore *girarAntiHorario(Arvore *raiz)
{
    Arvore *no = raiz->direita;
    raiz->direita = no->esquerda;
    no->esquerda = raiz;
    no->cor = raiz->cor;
    raiz->cor = VERMELHO;
    return no;
}

Arvore *moverNoVermelhoParaEsquerda(Arvore *raiz)
{
    trocarCor(raiz);
    if (cor(raiz->direita->esquerda) == VERMELHO)
    {
        raiz->direita = girarHorario(raiz->direita);
        raiz = girarAntiHorario(raiz);
        trocarCor(raiz);
    }
    return raiz;
}

Arvore *moverNoVermelhoParaDireita(Arvore *raiz)
{
    trocarCor(raiz);
    if (cor(raiz->esquerda->esquerda) == VERMELHO)
    {
        raiz = girarHorario(raiz);
        trocarCor(raiz);
    }
    return raiz;
}

Arvore *balancear(Arvore *raiz)
{
    //Nó vermelho é sempre filho à esquerda
    if (cor(raiz->direita) == VERMELHO)
        raiz = girarAntiHorario(raiz);

    //filho da direita e neto da esquerda são vermelhos
    if (raiz->esquerda != NULL && cor(raiz->esquerda) == VERMELHO && cor(raiz->esquerda->esquerda) == VERMELHO)
        raiz = girarHorario(raiz);

    //2 filhos vermelhos: trocar cor!
    if (cor(raiz->esquerda == VERMELHO) && cor(raiz->direita) == VERMELHO)
        trocarCor(raiz);
    return raiz;
}

Arvore *criarNo(int n)
{
    Arvore *no = (Arvore *)calloc(1, sizeof(Arvore));
    no->info = n;
    no->esquerda = NULL;
    no->direita = NULL;
    no->cor = VERMELHO;
    return no;
}

Arvore *removerMenor(Arvore *raiz)
{
    if (raiz->esquerda == NULL)
    {
        free(raiz);
        return NULL;
    }
    if (cor(raiz->esquerda) == PRETO && cor(raiz->esquerda->esquerda) == PRETO)
        raiz = moverNoVermelhoParaEsquerda(raiz);
    raiz->esquerda = removerMenor(raiz->esquerda);
    return balancear(raiz);
}

Arvore *procuraMenor(Arvore *atual)
{
    Arvore *no1 = atual;
    Arvore *no2 = atual->esquerda;
    while (no2 != NULL)
    {
        no1 = no2;
        no2 = no2->esquerda;
    }
    return no1;
}

Arvore *remover(Arvore *raiz, int valor)
{
    if (valor < raiz->info)
    {
        if (cor(raiz->esquerda) == PRETO && cor(raiz->esquerda->esquerda) == PRETO)
            raiz = moverNoVermelhoParaEsquerda(raiz);
        raiz->esquerda = remover(raiz->esquerda, valor);
    }
    else
    {
        if (cor(raiz->esquerda) == VERMELHO)
            raiz = girarHorario(raiz);
        if (valor == raiz->info && raiz->direita == NULL)
        {
            free(raiz);
            return NULL;
        }
        if (cor(raiz->direita) == PRETO && cor(raiz->direita->esquerda) == PRETO)
            raiz = moverNoVermelhoParaDireita(raiz);
        if (valor == raiz->info)
        {
            Arvore *aux = procuraMenor(raiz->direita);
            raiz->info = aux->info;
            raiz->direita = removerMenor(raiz->direita);
        }
        else
            raiz->direita = remover(raiz->direita, valor);
    }
    return balancear(raiz);
}

int removerNoVermelhoPreto(Arvore **raiz, int valor)
{
    *raiz = remover(*raiz, valor);
    if (*raiz != NULL)
        (*raiz)->cor = PRETO;
}

void imprimirColorida(Arvore *raiz)
{
    if (raiz->cor == VERMELHO)
    {
        printf("\x1b[31m%d\033[0m", raiz->info);
    }
    else
    {
        printf("%d", raiz->info);
    }
}

void imprimirArvore(Arvore *raiz)
{
    if (raiz != NULL)
    {
        imprimirColorida(raiz);
        printf(" ");
        if (raiz->esquerda != NULL)
            imprimirColorida(raiz->esquerda);
        else
            printf(" - ");
        printf(",");
        if (raiz->direita != NULL)
            imprimirColorida(raiz->direita);
        else
            printf(" - ");
        printf("\n");
        imprimirArvore(raiz->esquerda);
        imprimirArvore(raiz->direita);
    }
}

Arvore *inserirNo(Arvore *raiz, Arvore *no)
{
    if (raiz == NULL)
    {
        return no;
    }

    else
    {
        if (no->info < raiz->info)
        {
            raiz->esquerda = inserirNo(raiz->esquerda, no);
        }
        else if (no->info > raiz->info)
        {
            raiz->direita = inserirNo(raiz->direita, no);
        }
        if (cor(raiz->direita) == VERMELHO && cor(raiz->esquerda) == PRETO)
            raiz = girarAntiHorario(raiz);
        if (cor(raiz->esquerda) == VERMELHO && cor(raiz->esquerda->esquerda) == VERMELHO)
            raiz = girarHorario(raiz);
        if (cor(raiz->esquerda) == VERMELHO && cor(raiz->direita) == VERMELHO)
            trocarCor(raiz);

        return raiz;
    }
}

void inserirNoVermelhoPreto(Arvore **raiz, Arvore *no)
{
    *raiz = inserirNo(*raiz, no);
    if ((*raiz) != NULL)
    {
        (*raiz)->cor = PRETO;
    }
}

void main()
{
    Arvore *raiz = NULL;

    inserirNoVermelhoPreto(&raiz, criarNo(500));
    inserirNoVermelhoPreto(&raiz, criarNo(100));
    inserirNoVermelhoPreto(&raiz, criarNo(200));
    inserirNoVermelhoPreto(&raiz, criarNo(800));
    inserirNoVermelhoPreto(&raiz, criarNo(300));
    inserirNoVermelhoPreto(&raiz, criarNo(400));
    inserirNoVermelhoPreto(&raiz, criarNo(350));

    imprimirArvore(raiz);
    removerNoVermelhoPreto(&raiz, 800);
    printf("\n");
    imprimirArvore(raiz);
}
